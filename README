short version:
This repository is about a test-bed for the 2012 patchset from
Ondrej Kozina: "Allow user w/o CAP_SYS_ADMIN to submit ioctl commands into DM"
https://www.redhat.com/archives/dm-devel/2012-May/msg00011.html
https://www.redhat.com/archives/dm-devel/2012-May/msg00321.html


long version:

It's about an unprivileged user capability to do be:
$ cryptsetup luksFormat --key-file - /tmp/file.bin <<<key
$ echo "/tmp/file.bin    /tmp/dir   crypt  users,fsk_cipher=none,crypto_name=test,keyfile=/dev/stdin" >> ~/.fstab
$ mount --fstab ~/.fstab /tmp/dir <<<key  # both /tmp/dir and /tmp/file.bin being owned by us

but 2 things are needed:
# 1) unprivileged mount support (nosuid,nodev)
# 2) unprivileged loop-control/dm-control kernel support

1) is out of our scope, see:
http://thread.gmane.org/gmane.linux.kernel/654806
http://lwn.net/Articles/531114/
http://git.kernel.org/cgit/utils/util-linux/util-linux.git/log/libmount




2) is interesting anyway, because it's about allowing unprivileged user to:
$ dmsetup info
$ dmsetup table # without --showkeys
$ crypsetup status
$ crypsetup luksOpen
$ crypsetup luksClose

thus a more autonomous configuration and less suid needs (eg: cryptmount),
even if mounting the device itself still depends on a system-wide /etc/fstab


But for this to work it also needs:
# chgrp disk /dev/mapper/control && chmod g+rw /dev/mapper/control
# usermod -a -G disk our_user

** this would make the dm controller unsecure since powerful ioctl
   become available to users from the "disk" group **

Currently, one way to have unprivileged device mapper use is using:
# setcap cap_dac_override,cap_sys_admin+ep /sbin/dmsetup
or granting sudo's

The ioctl danse "expected" can be shown by (unpatched kernel):
# setcap cap_sys_ptrace+ep /usr/bin/strace
# setcap cap_dac_override,cap_sys_admin+ep /sbin/dmsetup
$ strace -e ioctl /sbin/dmsetup -c info



Thus, since filesystem permissions can't provide us with both power and security,
then the dm kernel module needs to implements the security at the ioctl level.


The new dm module parameter, accessible at boot is dm_mod.enable_security=1



=== notes about debugging using qemu ===

way 1)
$ sudo mount -o loop,sync,users,rw -t ext4 output/images/rootfs.ext2 /tmp/live-mod-image
This does not work (corrupt the filesystem)

way 2)
$ sudo tunctl -u our_user -t tap0
$ sudo ifconfig tap0 10.0.0.1 netmask 255.255.255.0 up
$ qemu-system-x86_64 -kernel output/images/bzImage -drive file=output/images/rootfs.ext2,if=ide -append "root=/dev/sda console=ttyS0" -net nic -net tap,ifname=tap0,script=no -display none -serial mon:stdio

In order to debug the module using gdb (ref: http://0x80.org/blog/debugging-kernel-modules/)

$ qemu-system-x86_64 -sS -above-params^^

# other terminal:
gdb ./output/build/linux-custom/vmlinux -q
gdb $ target remote :1234
gdb $ continue

# back in qemu:
qemu # cat /sys/module/dm_mod/sections/.text
qemu # cat /sys/module/dm_crypt/sections/.text
qemu # # or use the "add" shellscript in /home/default (from buildroot)

# back in gdb (host)
gdb $ set architecture i386:x86-64:intel	# may be needed as soon as after the first "continue"
gdb $ add-symbol-file ./output/build/linux-custom/drivers/md/dm-mod.ko 0xbla
gdb $ add-symbol-file ./output/build/linux-custom/drivers/md/dm-crypt.ko 0xfoo
gdb $ break dm_table_add_target
gdb $ break md_alloc
gdb $ c

# back in qemu:
qemu $ echo x | cryptsetup -v luksOpen --key-file - test.bin test


== updating quickly the module from the host (care out not corrupting the ext4 filesystem live running live ==
$ scp ./output/build/linux-custom/drivers/md/dm-{mod,crypt}.ko root@10.0.0.2:/lib/modules/3.13.0/kernel/drivers/md/
qemu # sudo remove
qemu # sudo add

And sadly we have to reset gdb symbols again since sections/.text changed.
