/* stripped down code borrowed from LVM2 to test lowlevel ioctl
   gcc -Iinclude -Ilibdm -Ilibdm/ioctl ioctl_user.c -ldevmapper -o ioctl-test
   sudo ./ioctl-test info */

#include <stdint.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// _IOWR
#include <sys/ioctl.h>
#include <linux/types.h>

#include "configure.h"
#include "libdm/libdm-common.h"
#include "dmlib.h"
#include "libdm-targets.h"
#include "libdm-common.h"
#include "dm-ioctl.h"


static struct cmd_data _cmd_data_v4[] = {
        {"create",      DM_DEV_CREATE,          {4, 0, 0}},
        {"reload",      DM_TABLE_LOAD,          {4, 0, 0}},
        {"remove",      DM_DEV_REMOVE,          {4, 0, 0}},
        {"remove_all",  DM_REMOVE_ALL,          {4, 0, 0}},
        {"suspend",     DM_DEV_SUSPEND,         {4, 0, 0}},
        {"resume",      DM_DEV_SUSPEND,         {4, 0, 0}},
        {"info",        DM_DEV_STATUS,          {4, 0, 0}},
        {"deps",        DM_TABLE_DEPS,          {4, 0, 0}},
        {"rename",      DM_DEV_RENAME,          {4, 0, 0}},
        {"version",     DM_VERSION,             {4, 0, 0}},
        {"status",      DM_TABLE_STATUS,        {4, 0, 0}},
        {"table",       DM_TABLE_STATUS,        {4, 0, 0}},
        {"waitevent",   DM_DEV_WAIT,            {4, 0, 0}},
        {"names",       DM_LIST_DEVICES,        {4, 0, 0}},
        {"clear",       DM_TABLE_CLEAR,         {4, 0, 0}},
        {"mknodes",     DM_DEV_STATUS,          {4, 0, 0}},
        {"versions",    DM_LIST_VERSIONS,       {4, 1, 0}},
        {"message",     DM_TARGET_MSG,          {4, 2, 0}},
        {"setgeometry", DM_DEV_SET_GEOMETRY,    {4, 6, 0}}
};


#define READ_IOCTL _IOR(DM_IOCTL, 0, int)
#define WRITE_IOCTL _IOW(DM_IOCTL, 1, int)

// #define READ_IOCTL _IOR(DM_IOCTL, 0, int)
// #define DM_VERSION       _IOWR(DM_IOCTL, DM_VERSION_CMD, struct dm_ioctl)

int main(int argc, char **argv) {
	if(argc < 2) {
		fprintf(stderr, "%s <ioctl-name>\n", argv[0]);
		return 1;
	}
	// printf("DM_DEV_STATUS_CMD (%d) => DM_DEV_STATUS (%lu)\n", DM_DEV_STATUS_CMD,  _IOWR(DM_IOCTL, DM_DEV_STATUS_CMD, struct dm_ioctl));

	uint32_t cmd = -1; const int (*version)[3]; int i = 0;
	while(_cmd_data_v4[i].name) {
		if(! strcmp(_cmd_data_v4[i].name, argv[1])) {
			printf("found name %s (cmd = %4u)\n", _cmd_data_v4[i].name, _cmd_data_v4[i].cmd);
			cmd = _cmd_data_v4[i].cmd;
			version = &_cmd_data_v4[i].version;
			break;
		}
		i++;
	}

	if(cmd == -1) {
		fprintf(stderr, "can't find matching ioctl\n");
		return 1;
	}


	int fd = -1;
	if ((fd = open("/dev/mapper/control", O_RDWR)) < 0) {
		perror("open");
		return -1;
	}

	int len = 16 * 1024;
	char buf[200];
	struct dm_ioctl *dmi;
	if (!(dmi = dm_malloc(len)))
                return 1;
	memset(dmi, 0, len);
        dmi->version[0] = (*version)[0];
        dmi->version[1] = (*version)[1];
        dmi->version[2] = (*version)[2];
        dmi->data_size = len;
        dmi->data_start = sizeof(struct dm_ioctl);

	printf("sending %4u\n", cmd);
	if(ioctl(fd, cmd, dmi) < 0)
		perror("ioctl");

	if(cmd == DM_VERSION) {
		printf("dm, version %d,%d,%d\n", dmi->version[0], dmi->version[1], dmi->version[2]);
		return 0;
	}
        printf("dm %s %s[%u]: %s\n", dmi->name, dmi->uuid, dmi->data_size, dmi->data);
	// printf("message: %s\n", dmi);
	return 0;

}

